#!/bin/perl

# Program: soma11.pl
# Purpose: Program to get Genbank accession codes for SOMAscan aptamer constructs
# so that this can be submitted to Batch Entrez so seq entries can be stored locally
# Author: Danny Tuckwell (TUCKWDA1)
# Date: 05APR18
# Input: Somascan construct/aptamer annotaton: annot_v3.txt
# Output: List of accession codes: acc_list.txt
# Dependencies: 
# Assumptions:
# Modification history:
################################################################################

use strict;
use warnings;

### Read in annot_v3.txt SOMAscan data and create data for further processing

#Read in data
my $infile = 'annot_v3.txt';

my @annot; #for reading data into
my @accId; #array with acc id

open my $info, $infile
   or die 'no such number, no such zone\n';

#read data into array
while (my $line = <$info>){
    chomp $line;
    $line =~ s/\$//; #remove dollar which appears in some lines;
    #print "$line\n";
    push @annot, $line;
}

close $infile;

#hash of arrays with ID, start and end;
foreach my $line (@annot){
   #pull out the id value from annot
   my @line = split( /\t/, $line); # split each cell of annot into array element;    
   if($line[10] =~ /-((NP_\w+|NP_\w+\.\d)-\w+-\d+-(\d+)-(\d+))\b/){  
      my $seqId = $2; #acc_id;
      $seqId =~ s/\.[0-9]//; #remove .1 or .2 which are the version number and are not present in acc code   
      #print "$seqId\n"; 
      push @accId, "$seqId";
   }else{
      if (!($line[10] eq 'NA')){
         print "$line[10]\n"; #check why rejected;
      }
   }
}

#Print HoA to check
#for my $test (keys %annot) {
#   print "$test: @{ $annot{$test} }\n";
#}

 
  
#File for output
open my $outfile, '>acc_list.txt';

foreach my $val1 (@accId){
    print $outfile "$val1",',';
}
print $outfile "\n";
close $outfile;



######################### END OF PROGRAM ######################;

