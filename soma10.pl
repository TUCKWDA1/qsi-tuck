#!/bin/perl

# Program: soma10.pl
# Purpose: Program to identify SOMAscan aptamer constructs which bind 
# to predicted intracellular regions
# Author: Danny Tuckwell (TUCKWDA1)
# Date: 26MAR18
# Input: Somascan construct/aptamer annotaton: annot_v3.txt
# Output: List of hits with intracellular aptamers and hits which are 
#         multi-TM-spanning: soma_out.txt
# Dependencies: BioPerl
# Assumptions:
# Modification history:
################################################################################

use strict;
use warnings;
use Bio::Seq;
use Bio::DB::GenBank;


### Read in annot_v3.txt SOMAscan data and create data for further processing

#Read in data
my $infile = 'annot_v3.txt';

my @annot; #for reading data into
my %annot; #hash of arrays with acc id and construct details

open my $info, $infile
   or die 'no such number, no such zone\n';

#read data into array
my $linecount; #count so that I can limit input size if necessary;
while (my $line = <$info>){
    $linecount ++;
    if ($linecount >100){last};
    chomp $line;
    $line =~ s/\$//; #remove dollar which appears in some lines;
    #print "$line\n";
    push @annot, $line;
}

close $infile;

#hash of arrays with ID, start and end;
foreach my $line (@annot){
   #pull out the id-length-start-end value from annot
   my @line = split( /\t/, $line); # split each cell of annot into array element;  
      
   if($line[10] =~ /-((NP_\w+)-\w+-\d+-(\d+)-(\d+))\b/){     
      #print "$1\n";
      #extract ID, start and end
      #print "$2\n";
      #print "$3\n";
      #print "$4\n";
      push @{$annot{$line[0]}}, "$2", "$3", "$4";
   }
}

#Print HoA to check
#for my $test (keys %annot) {
#   print "$test: @{ $annot{$test} }\n";
#}

   
### Step through hash , get data for each acc code and see if construct is down stream of TM


#create hash to put hits in;
my %hits;

#my GBcount;
#my GBmax=10;

foreach my $query (keys %annot) {
   #while GBcount > GBmax then wait;
  
   my $hit_string;  #term to go in hits hash;

   print  "$query: @{ $annot{$query}  }\n";
   #print $outfile "$query \t $annot{$query}[0]\t $annot{$query}[1] \t $annot{$query}[2] \t";
   
   #Create object
   my $db_obj = Bio::DB::GenBank->new;

   #set up variables from hash;
   #print "$annot{$query}[0]\n";

   my $seqId = $annot{$query}[0]; # acc id;
   $seqId =~ s/\.[0-9]//; #remove .1 or .2 which are the version number and are not present in acc code;

   #GPcount ++;     
   my $seq_obj = $db_obj -> get_Seq_by_acc("$seqId");
   #GPcount --;

   $hit_string = join "", $query, "\t", $annot{$query}[0], "\t", $annot{$query}[1], "\t", $annot{$query}[2], "\t", 
                            $seq_obj -> accession_number(), "\t", $seq_obj -> length(), "\t";

   my $somStart = $annot{$query}[1]; #somamer start;
   my $somEnd   = $annot{$query}[2]; #somamer end;

   #create variables for analysis;
   #my $sigEnd;
   my $TMStart;
   my $TMEnd;
   

   ### Get protein information from NCBI ###;
   #print $seq_obj -> seq();
   #print "\n";
   #print $seq_obj -> desc();
   #print "\n";
   #print $seq_obj -> accession_number(), "\t", $seq_obj -> length(), "\t";

   my $TMcount = 0; #in case there is more than one TM;

   for my $feat_obj ($seq_obj->get_SeqFeatures){
      #Signal peptide
      #if ($feat_obj->primary_tag =~ /sig_peptide/){ 
      #   print $feat_obj->primary_tag, 
      #      ": start ", $feat_obj->start, " to ", $feat_obj->end;
      #   $sigEnd=$feat_obj->end;  
      #}

      #TM region
      if ($feat_obj->primary_tag =~ /Site/i){
         for my $tag ($feat_obj -> get_all_tags){
           if ($tag =~ /site_type/i){
              #print "   tag: ", $tag, "\n";
              for my $value ($feat_obj -> get_tag_values($tag)){
                 if ($value =~ /transmembrane region/i){
                    $TMcount ++;
                    #print "value: ", $value, ": start ", $feat_obj->start, " to ", $feat_obj->end, "\n";
                    #print $outfile "\tTM:\t", $feat_obj->start, "\t", $feat_obj->end;     
                    $hit_string = join "", $hit_string, "\t TM: \t", $feat_obj->start, "\t", $feat_obj->end;    
                             $TMStart = $feat_obj->start;
                    $TMEnd = $feat_obj->end;
                    
                 }
              }
           }
        }   
      }
   }


   ### Compare database information with Somamer data ###;
   #if (defined $sigEnd && $somStart < $sigEnd) { 
   #   print "Construct starts within Signal Peptide\n";
   #}
   if ($TMcount gt 1){
       #print $outfile "\tMore than one TM\n";
       $hit_string = join "", $hit_string, "\tMore than one TM\n";     
   }elsif(defined $TMStart && defined $TMEnd){
      if ($somStart > $TMStart) {
         #print $outfile "\tConstruct is potentially intracellular\n";
         $hit_string .= "\tConstruct is potentially intracellular\n";
      }else{
         #print $outfile "\tConstruct ends before TM region\n";
         $hit_string .= "\tConstruct ends before TM region\n";    
      }
   }else{ 
      #print $outfile "\tNo TM defined\n";
      $hit_string .= "\tNo TM defined\n"; 
   };
   #print $outfile "\n";
   print "$hit_string\n"; # to check;
   $hits{$query}=$hit_string; #put hit string into hash;
   sleep(1);

}

#File for output
open my $outfile, '>soma_out.txt';

foreach my $key1 (sort keys %hits){
    print $outfile "$hits{$key1}";
}

close $outfile;



######################### END OF PROGRAM ######################;

